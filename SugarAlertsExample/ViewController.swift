//
//  ViewController.swift
//  SugarAlertsExample
//
//  Created by Masahiko Tsujita on 2017/04/16.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit
import SugarAlerts

class ViewController: UIViewController {

    @IBOutlet weak var alertStyleSegmentedControl: UISegmentedControl!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func showAlert(_ sender: UIButton) {
        if alertStyleSegmentedControl.selectedSegmentIndex == 0 {
            alert(title: "タイトル", message: "メッセージ")
                .addTextField(placeholder: "ユーザーID")
                .addSecureTextField(placeholder: "パスワード")
                .addAction(title: "アクション1") { _ in
                    print("アクション1 is selected.")
                }
                .addAction(title: "アクション2", style: .destructive) { _ in
                    print("アクション2 is selected.")
                }
                .addCancelAction()
                .present(from: self)
        } else {
            actionSheet(title: "タイトル", message: "メッセージ")
                .addAction(title: "アクション1") { _ in
                    print("アクション1 is selected.")
                }
                .addAction(title: "アクション2", style: .destructive) { _ in
                    print("アクション2 is selected.")
                }
                .anchorPopover(to: (view: sender, rect: sender.bounds), permittedArrowDirections: [.up, .down])
                .addCancelAction()
                .present(from: self)
        }
    }

    @IBAction func showAlertFromBarButtonItem(_ sender: UIBarButtonItem) {
        actionSheet(title: "タイトル", message: "メッセージ")
            .addAction(title: "アクション1") { _ in
                print("アクション1 is selected.")
            }
            .addAction(title: "アクション2", style: .destructive) { _ in
                print("アクション2 is selected.")
            }
            .anchorPopover(to: sender)
            .addCancelAction()
            .present(from: self)
    }

}

