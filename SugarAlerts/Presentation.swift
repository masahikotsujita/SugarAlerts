//
//  Presentation.swift
//  SugarAlerts
//
//  Created by Masahiko Tsujita on 2017/04/18.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit

public class AlertControllerPresentation {

    private(set) public var alertController: UIAlertController

    init(alertController: UIAlertController) {
        self.alertController = alertController
    }

    public func present(from source: UIViewController, completion: (() -> Void)? = nil) {
        source.present(alertController, animated: true, completion: completion)
    }

}

public class AlertPresentation: AlertControllerPresentation {}

public class ActionSheetPresentation: AlertControllerPresentation {}

public extension AlertControllerPresentation {

    public func addAction(title: String?, style: UIAlertActionStyle = .default, handler: ((UIAlertController) -> Void)?) -> Self {
        let alertController = self.alertController
        alertController.addAction(UIAlertAction(title: title, style: style, handler: handler.flatMap { handler in { _ in handler(alertController) } }))
        return self
    }

    public func addCancelAction(handler: ((UIAlertController) -> Void)? = nil) -> Self {
        let alertController = self.alertController
        alertController.addAction(UIAlertAction(title: cancelActionTitleHandler?(), style: .cancel, handler: handler.flatMap { handler in { _ in handler(alertController) } }))
        return self
    }

}

public extension AlertPresentation {

    public func addTextField(placeholder: String? = nil, additionalConfiguration: ((UITextField) -> Void)? = nil) -> Self {
        alertController.addTextField { textField in
            if let placeholder = placeholder {
                textField.placeholder = placeholder
            }
            additionalConfiguration?(textField)
        }
        return self
    }

    public func addSecureTextField(placeholder: String? = nil, additionalConfiguration: ((UITextField) -> Void)? = nil) -> Self {
        alertController.addTextField { textField in
            if let placeholder = placeholder {
                textField.placeholder = placeholder
            }
            textField.isSecureTextEntry = true
            additionalConfiguration?(textField)
        }
        return self
    }

}

public extension ActionSheetPresentation {

    public func configurePopover(_ configuration: (UIPopoverPresentationController) -> Void) -> Self {
        if let popover = alertController.popoverPresentationController {
            configuration(popover)
        }
        return self
    }

    public func anchorPopover(to source: (view: UIView, rect: CGRect), permittedArrowDirections: UIPopoverArrowDirection = .any) -> Self {
        return configurePopover { popover in
            popover.sourceView = source.view
            popover.sourceRect = source.rect
            popover.permittedArrowDirections = permittedArrowDirections
        }
    }

    public func anchorPopover(to barButtonItem: UIBarButtonItem, permittedArrowDirections: UIPopoverArrowDirection = .any) -> Self {
        return configurePopover { popover in
            popover.barButtonItem = barButtonItem
            popover.permittedArrowDirections = permittedArrowDirections
        }
    }

}
