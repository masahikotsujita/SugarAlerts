//
//  UIViewController+SugarAlerts.swift
//  SugarAlerts
//
//  Created by Masahiko Tsujita on 2017/04/18.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import UIKit

public func alert(title: String?, message: String?) -> AlertPresentation {
    return AlertPresentation(
        alertController: UIAlertController(title: title, message: message, preferredStyle: .alert)
    )
}

public func actionSheet(title: String?, message: String?) -> ActionSheetPresentation {
    return ActionSheetPresentation(
        alertController: UIAlertController(title: title, message: message, preferredStyle: .actionSheet)
    )
}
