//
//  Localization.swift
//  SugarAlerts
//
//  Created by Masahiko Tsujita on 2017/04/21.
//  Copyright © 2017年 Masahiko Tsujita. All rights reserved.
//

import Foundation

public var cancelActionTitleHandler: (() -> String?)? = { "Cancel" }
