# SugarAlerts

Simpler way to create and present `UIAlertController`s.

## Install

### Cathage:

```
git "git@gitlab.com:masahikotsujita/SugarAlerts.git"
```

## Usage

### Overview

You can create and present alerts and action sheets easily with method chaining as follows:

#### Alert

```swift
alert(title: "Confirm", message: "Would you like to proceed?")
    .addedAction(title: "OK") { alert in
        // ...
    }
    .addedCancelAction()
    .presented(by: self)
```

#### Action Sheet

```swift
actionSheet(title: "Confirm Logout", message: "Would you like to logout?")
    .addAction(title: "Logout", style: .destructive) { _ in
        // ...
    }
    .addCancelAction()
    .present(from: self)
```

### Text Fields

```swift
alert(title: "Login", message: "Enter your user id and password.")
    .addTextField(placeholder: "User ID")
    .addSecureTextField(placeholder: "Password")
    .addAction(title: "Login") { alert in
        let userID = alert.textFields![0].text!
        let password = alert.textFields![1].text!
        // ...
    }
    .addCancelAction()
    .present(from: self)
```

### Configuring Popover Presentation Controllers

On iPad, displaying `UIAlertController` as action sheet without configuring `popoverPresentationController` leads run-time crash.
To avoid it, you must set correct values to `sourceView` and `sourceRect` or `barButtonItem` of `UIAlertController.popoverPresentationController`.
SugarAlerts provides some functions to do this easily:

- `configuredPopover(_:)`
- `anchoredPopover(to: permittedArrowDirections:)`

#### Example

```swift
@IBAction func showAlertFromBarButtonItem(_ sender: UIBarButtonItem) {
    actionSheet(title: "Confirm Logout", message: "Would you like to logout?")
        .addAction(title: "Logout", style: .destructive) { _ in
            // ...
        }
        .addCancelAction()
        .anchorPopover(to: sender)
        .present(from: self)
}
```

## License

SugarAlerts is distributed under the MIT license. See [LICENSE](./LICENSE) for more details.
